package app;

import model.Product;
import org.xml.sax.SAXException;
import utils.JAXBRead;
import utils.JAXBWrite;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SacomProcess {

    public static final String INPUT = "../SACOM_JAXB/data/";
    public static final String PROCESSED = "../SACOM_JAXB/processed";
    public static final String OUTPUT = "../SACOM_JAXB/output/";

    public static void main(String[] args) throws IOException, XMLStreamException, ParserConfigurationException, SAXException, JAXBException {

        File[] files = new File(INPUT).listFiles();
        if (isDirEmpty(INPUT)) {
            System.out.println("No orders to process");
        }

        //process each file found in the data directory
        for (File ordersFile : files) {
            String orderCount = getOrdersFileIndex(ordersFile.getName());
            Map<String, List<Product>> prodHashmap = new HashMap<>();

            //iterate through list of products returned from JAXBRead
            for (Product product : new JAXBRead().getProductsFromXML(ordersFile.getPath())) {
               // System.out.println(product);
                addToList(prodHashmap, product.getSupplier(), product);
            }

            for(Map.Entry<String, List<Product>> entry : prodHashmap.entrySet()){

                System.out.println(entry.getKey() + " -> " + entry.getValue());
                System.out.println("........total products in supplier: " + entry.getValue().size());
            }

            //select products by supplier and write the to specific file
            for (String key : prodHashmap.keySet()) {
                new JAXBWrite().createDocument(prodHashmap.get(key), OUTPUT + key + orderCount + ".xml");
            }
            //move processed orders to processed directory
            File processedDir = new File(PROCESSED);
            if(!processedDir.exists()){
                processedDir.mkdir();
            }
            Files.move(Paths.get(INPUT + ordersFile.getName()),Paths.get(PROCESSED + "/" + ordersFile.getName()), StandardCopyOption.REPLACE_EXISTING);

        }
    }

    private static boolean isDirEmpty(final String directory) throws IOException {
        try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(directory))) {
            return !dirStream.iterator().hasNext();
        }
    }

    //get the ending numbers from the input file
    private static String getOrdersFileIndex(String filename) {
        Matcher matcher = Pattern.compile("(\\d+)").matcher(filename);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    //helper method to map the products
    public static synchronized void addToList(Map<String, List<Product>> prodHashmap, String mapKey, Product myItem) {
        List<Product> itemsList = prodHashmap.get(mapKey);

        // if list does not exist create it
        if (itemsList == null) {
            itemsList = new ArrayList<>();
            itemsList.add(myItem);
            prodHashmap.put(mapKey, itemsList);
        } else {
            // add if item is not already in list
            if (!itemsList.contains(myItem)) itemsList.add(myItem);
        }
    }
}
