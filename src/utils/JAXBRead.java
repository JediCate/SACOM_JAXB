package utils;

import model.Order;
import model.Orders;
import model.Product;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JAXBRead {

    public List<Product> getProductsFromXML(String filename) throws IOException, JAXBException {

        JAXBContext context = JAXBContext.newInstance(Orders.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        Orders products = (Orders) unmarshaller.unmarshal(new File(filename));
        List<Order> orderList = products.getOrderList();
        List<Product> productList = new ArrayList<>();
        for (Order order : orderList) {
           List<Product> perOrder = order.getProducts();
           for(Product prod : perOrder){
               productList.add(new Product(prod.getSupplier(), prod.getDescription(), prod.getGtin(), prod.getPrice(), order.getId()));
           }
        }
        return productList;
    }

}
