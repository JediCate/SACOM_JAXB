package utils;

import model.Product;
import model.Products;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.List;

public class JAXBWrite {

    public void createDocument(List<Product> productList, String filename) throws JAXBException {

        Products products = new Products();
        products.setProductsList(productList);

        JAXBContext context = JAXBContext.newInstance(Products.class);
        Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(products, new File(filename));

    }
}
