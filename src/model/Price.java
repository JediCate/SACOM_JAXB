package model;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class Price {

    @XmlValue
    private BigDecimal value;

    @XmlAttribute(name = "currency")
    private String currency;

    Price(){}

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
    public void setPrice(String price) {
        this.value = new BigDecimal(price);
    }

    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
