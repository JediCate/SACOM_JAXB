package model;

import javax.xml.bind.annotation.*;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {
    @XmlAttribute(name = "ID")
    private long id;
    @XmlAttribute
    private Date created;

    @XmlElement(name = "product", type= Product.class)
    private List<Product> products;

    public Order(){

    }

    public Order(long id, Date created, List<Product> products){
        this.id = id;
        this.created = created;
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }
    public void setProducts(List<Product> products) {
        this.products = products;
    }


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }


    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
        return "Order [id=" + id + ", created=" + df.format(created) + ", " + products.toString() +  "]\n";
    }
}
