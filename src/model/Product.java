package model;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"description", "gtin", "price", "supplier", "orderId"})
public class Product {
    private String description;
    private String gtin;
    private Price price;
    private long orderId;
    private String supplier;


    public Product(){
    }

    public Product(String supplier, String description, String gtin, Price price, long orderId){
        this.supplier = supplier;
        this.description = description;
        this.gtin = gtin;
        this.price = price;
        this.orderId = orderId;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getGtin() {
        return gtin;
    }
    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public Price getPrice() {
        return price;
    }
    public void setPrice(Price price) {
        this.price = price;
    }

    public String getSupplier() {
        return supplier;
    }
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public long getOrderId() {
        return orderId;
    }
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Product [description=" + description + ", gtin=" + gtin + ", price=" + price.getValue()
                + ", currency =" + price.getCurrency() + ", supplier=" + supplier + ", orderId=" + orderId + "]";
    }
}
