The app checks if the data folder is empty.
If not empty it reads each file in the folder using the DOM parser.
I used JAXB for read and write.
With the information from the order files the program creates a list of products using the Product model.
Next the program will iterate the list of products and map it by supplier.
For each supplier key in the map the program will write a file with the products list from the
corresponding map's valueSet.
After the program has finished processing, the file will pe moved to the processed directory.

I have put two input sample files in the data directory.
The paths should work just fine, as they are dependent on the App directory (SACOM_JAXB), 
so it doesn't matter how your computer is partitioned.
I haven't used any additional libraries.

For writing this program I've used Intellij IDEA 2017.2.6 and Java SE 8.